//
//  Config.h
//  Lab
//
//  Created by Daniel Lundqvist on 2013-05-02.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SERVER_URL @"http://localhost:5984"
#define DB_NAME @"lab"

@interface Config : NSObject

@end
