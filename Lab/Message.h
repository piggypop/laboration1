//
//  Message.h
//  Lab
//
//  Created by Daniel Lundqvist on 2013-09-05.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *_rev;
@property (nonatomic) NSString *body;
@property (nonatomic) NSString *recipient;
@property (nonatomic) NSString *type;

-(id)initWithDict: (NSDictionary *)dict;
- (NSDictionary *)createDictionaryFromData;

@end
