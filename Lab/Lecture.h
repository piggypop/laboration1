//
//  Lecture.h
//  Lab
//
//  Created by Daniel Lundqvist on 2013-05-02.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Lecture : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *_rev;
@property (nonatomic) NSString *room;
@property (nonatomic) NSString *teacher;
@property (nonatomic) NSString *course;
@property (nonatomic) NSString *notes;
@property (nonatomic) NSString *createdBy;
@property (nonatomic) NSString *type;

- (NSDictionary *)createDictionaryFromData;
- (id)initWithDict: (NSDictionary *)dict;

@end
