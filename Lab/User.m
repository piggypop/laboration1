//
//  Student.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-04-09.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "User.h"

@implementation User

- (NSDictionary *)createDictionaryFromData {
    return @{
         @"_id": self._id,
         @"course": self.course ? self.course : @"",
         @"role": self.role,
         @"type": @"USER"
    };
}

-(void)set_id:(NSString *)_id
{
    if ([_id isEqualToString:@""]) {
        NSLog(@"A user must have an ID!");
    }
    __id = _id;
}


@end
