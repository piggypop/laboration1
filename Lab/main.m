//
//  main.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-04-09.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//
#import <CouchCocoa/CouchCocoa.h>
#import <Foundation/Foundation.h>
#import "User.h"
#import "DB.h"
#import "Lecture.h"
#import "Message.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // Create access to database
        DB *db = [DB new];
        
        // Create users
        User *admin = [[User alloc]init];
        admin._id = @"Admin";
        admin.role = @"ADMIN";
        
        // Create new lectures, using the Admin user
        Lecture *monday = [[Lecture alloc]init];
        monday._id = @"Monday";
        monday.room = @"1";
        monday.teacher = @"Yoda";
        monday.course = @"Programming";
        monday.notes = @"Läs s. 123-234";
        monday.createdBy = admin._id;
        [db save:monday];
        
        Lecture *wednesday = [[Lecture alloc]init];
        wednesday._id = @"Wednesday";
        wednesday.room = @"2";
        wednesday.teacher = @"Yoda";
        wednesday.course = @"Programming";
        wednesday.notes = @"Läs s. 223-334";
        wednesday.createdBy = admin._id;
        [db save:wednesday];
        
        // Fetch previously stored Lecture from database
        Lecture *lectureToModify = [[Lecture alloc] initWithDict:[db getByID:monday._id]];
        
        // Make modifications to lecture
        lectureToModify.teacher = @"Bobby";
        
        // Update the lecture
        [db save:lectureToModify];
        
        
        User *anna = [[User alloc]init];
        anna._id = @"Anna";
        anna.course = @"Programming";
        anna.role = @"STUDENT";
        
        User *kalle = [[User alloc]init];
        kalle._id = @"Kalle";
        kalle.course = @"Programming";
        kalle.role = @"STUDENT";
        
        
        // Store users in database
        [db save:admin];
        [db save:anna];
        [db save:kalle];
        
        
        // Create new message to student
        Message *message = [Message new];
        message._id = @"TheSingleMessageID";
        message.body = @"Hello! Welcome Anna!";
        message.recipient = anna._id;
        
        // Save the message
        [db save:message];
        
        // Create new message to all students
        Message *messageToAll = [Message new];
        messageToAll._id = @"TheMessageToAllID";
        messageToAll.body = @"Hello Everyone! Say welcome to Anna, our new student!";
        messageToAll.recipient = @"ALL";
        
        // Save the message
        [db save:messageToAll];
        
        
        // Get Annas lecture for today
        Lecture *todaysLecture = [[Lecture alloc] initWithDict:[db getByID:@"Monday"]];
        if ([todaysLecture.course isEqualToString:anna.course]) {
            NSLog(@"\n*** Annas lecture for today ***\nCourse: %@\nTeacher: %@\nRoom: %@\nNotes: %@\n\n\n\n", todaysLecture.course, todaysLecture.teacher, todaysLecture.room, todaysLecture.notes);
        }
     
        // Get Kalles lecture for this week
        NSArray *thisWeek = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday"];
        for (NSString *day in thisWeek) {
            Lecture *currentLecture = [[Lecture alloc] initWithDict:[db getByID:day]];
            if ([currentLecture.course isEqualToString:kalle.course]) {
                NSLog(@"\n*** Kalles lectures for %@ ***\nCourse: %@\nTeacher: %@\nRoom: %@\nNotes: %@\n\n", day, currentLecture.course, currentLecture.teacher, currentLecture.room, currentLecture.notes);
            }
        }
        
        // Get message for all students
        Message *messageForAll = [[Message alloc] initWithDict:[db getByID:@"TheMessageToAllID"]];
        NSLog(@"\nThis is a message for all students: %@", messageForAll.body);
        
    
    }
    return 0;
}

