//
//  DB.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-05-02.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "DB.h"

@implementation DB

- (BOOL)save: (id)object {
    
    NSURL* serverURL = [NSURL URLWithString: SERVER_URL];
    CouchServer *server = [[CouchServer alloc] initWithURL:serverURL];
    CouchDatabase *database = [server databaseNamed: DB_NAME];
    [database ensureCreated:nil];
    
    NSDictionary *documentProperties = [object performSelector:@selector(createDictionaryFromData)];

    CouchDocument* newDocument = [database documentWithID:[object performSelector:@selector(_id)]];
    RESTOperation* opCreate = [newDocument putProperties: documentProperties];
    
    [opCreate onCompletion: ^{
        if (opCreate.isSuccessful){
            NSLog(@"Successfully updated document! The new revision ID is: %@", [[opCreate resultObject] revisionID]);
        }
        else {
//            NSLog(@"Failed to update document, : %@", opCreate.error);
        }
    }];
    
    if (![opCreate wait]) {
//        NSLog(@"An error occurred writing the document with id: %@", newDocument.documentID);
        return false;
    }
    
    return true;
    
}

- (NSDictionary *)getByID:(NSString *)ID {
    
    NSURL* serverURL = [NSURL URLWithString: SERVER_URL];
    CouchServer *server = [[CouchServer alloc] initWithURL:serverURL];
    CouchDatabase *database = [server databaseNamed: DB_NAME];
    
    return [database documentWithID:ID].properties;
}

@end
