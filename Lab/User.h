//
//  Student.h
//  Lab
//
//  Created by Daniel Lundqvist on 2013-04-09.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *course;
@property (nonatomic) NSString *role;
@property (nonatomic) NSString *type;

- (NSDictionary *)createDictionaryFromData;


@end
