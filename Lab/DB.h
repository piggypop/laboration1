//
//  DB.h
//  Lab
//
//  Created by Daniel Lundqvist on 2013-05-02.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchCocoa/CouchCocoa.h>
#import "Config.h"

@interface DB : NSObject

- (BOOL)save:(id)object;
- (NSDictionary *) getByID:(NSString *)ID;

@end
