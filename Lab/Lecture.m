//
//  Lecture.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-05-02.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "Lecture.h"

@implementation Lecture

- (NSDictionary *)createDictionaryFromData {
    
    NSMutableDictionary *dict = [@{
             @"_id": self._id,
             @"room": self.room,
             @"teacher": self.teacher,
             @"course": self.course,
             @"notes": self.notes,
             @"createdBy": self.createdBy,
             @"type": @"LECTURE"
        } mutableCopy];
    
    if (self._rev) {
        [dict setValue:self._rev forKey:@"_rev"];
    }
    
    return dict;
    
}

-(id)initWithDict: (NSDictionary *)dict {
    self = [super init];
    if (self){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

-(void)setCourse:(NSString *)course
{
    if ([course isEqualToString:@""]) {
        NSLog(@"A course must have a name!");
    }
    _course = course;
}

-(void)set_id:(NSString *)_id
{
    if ([_id isEqualToString:@""]) {
        NSLog(@"A lecture must have an ID!");
    }
    __id = _id;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key {
    NSLog(@"Failed setting value %@ for undefined key %@", value, key);
}

@end
