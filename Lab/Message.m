//
//  Message.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-09-05.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "Message.h"

@implementation Message

-(id)initWithDict: (NSDictionary *)dict {
    self = [super init];
    if (self){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

- (NSDictionary *)createDictionaryFromData {
    return @{
             @"_id": self._id,
             @"body": self.body,
             @"recipient": self.recipient,
             @"type": @"MESSAGE"
             };
}

-(void)set_id:(NSString *)_id
{
    if ([_id isEqualToString:@""]) {
        NSLog(@"A message must have an ID!");
    }
    __id = _id;
}

-(void)setBody:(NSString *)body
{
    if ([body isEqualToString:@""]) {
        NSLog(@"A message must have content!");
    }
    _body = body;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key {
    NSLog(@"Failed setting value %@ for undefined key %@", value, key);
}

@end
