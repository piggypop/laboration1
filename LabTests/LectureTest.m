//
//  LectureTest.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-09-15.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "LectureTest.h"
#import "Lecture.h"

@implementation LectureTest
{
    Lecture *lecture;
}

- (void)setUp
{
    lecture = [Lecture new];
}

- (void)tearDown
{
    lecture = nil;
}

- (void)testInitWithDict
{
    NSDictionary *testDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"LectureDayTest", @"_id", @"1", @"room", @"Tester", @"teacher", @"Programming", @"course", @"Läs s. x-x", @"notes", @"Tester", @"createdBy", nil];
    
    Lecture *lecture = [[Lecture alloc] initWithDict:testDictionary];
    
    STAssertNotNil(lecture, @"The lecture should not be nil");
}

@end
