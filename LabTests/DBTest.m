//
//  DBTest.m
//  Lab
//
//  Created by Daniel Lundqvist on 2013-09-15.
//  Copyright (c) 2013 Daniel Lundqvist. All rights reserved.
//

#import "DBTest.h"
#import "DB.h"
#import "Message.h"
#import "User.h"

@implementation DBTest
{
    DB *db;
}

- (void)setUp
{
    db = [DB new];
}

- (void)tearDown
{
    db = nil;
}

- (void)testSave
{
    Message *message = [Message new];
    message._id = @"TestMessage";
    message.body = @"Hello, this is a test message";
    message.recipient = @"ALL";
    
    BOOL result = [db save:message];

    STAssertTrue(result, @"Saving to database should be possible");
}

- (void)testGetByID
{
    User *testUser = [[User alloc]init];
    testUser._id = @"TestUser";
    testUser.role = @"TESTER";
    
    [db save:testUser];
    
    NSDictionary *dict = [db getByID:@"TestUser"];
    
    BOOL result = [dict[@"_id"] isEqualToString:testUser._id];
        
    STAssertTrue(result, @"should be equal");
}





@end
